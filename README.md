### Code Input React [![Netlify Status](https://api.netlify.com/api/v1/badges/2bd20922-992d-4f35-8a8b-2527cad841e0/deploy-status)](https://app.netlify.com/sites/winterzz-dev-code-input/deploys)

---

*Read this in other languages: [Русский](README.ru.md)*

---

[Demo](https://winterzz-dev-code-input.netlify.app)

---

This repository is source code for `React` programming tutorial:

 - We will create input component for verification code
 - Integrate with `react-hook-form` library

---

![Image screen](https://i.postimg.cc/dVYWhX2V/SCR-20220723-nbe.png)

---

You can use any valid credentials and *REACT* confirmation code(there is only one allowed code,
because application uses mock verification service)
