import { Heading, Text } from "evergreen-ui";
import React from "react";

import { Cat, PageTemplate } from "../../components";
import { useMain } from "./use-main";

export default function Main() {
  useMain();

  return (
    <PageTemplate>
      <Heading size={700}>You found it!</Heading>
      <Text color="gray600" size={500}>
        It was amazing
      </Text>
      <Cat />
    </PageTemplate>
  );
}
