import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

import { useAuth } from "../../auth";
import { REGISTER_PATH } from "../../constants";
import { useDocumentTitle } from "../../hooks";

export function useMain() {
  useDocumentTitle("You found it!");
  const navigate = useNavigate();
  const { isAuthenticated, user, setIsAuthenticated, setUser } = useAuth();

  useEffect(() => {
    if (!isAuthenticated || !user) {
      setUser!(undefined);
      setIsAuthenticated!(false);
      navigate(REGISTER_PATH);
    }
  }, [isAuthenticated, navigate, setIsAuthenticated, setUser, user]);
}
