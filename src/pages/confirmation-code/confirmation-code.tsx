import { Button, Heading, Text } from "evergreen-ui";
import React from "react";
import { Controller } from "react-hook-form";

import { CodeInput, Form, PageTemplate } from "../../components";
import { ButtonContainer, ErrorText } from "./confirmation-code-styled";
import { CODE_LENGTH } from "./constants";
import { useConfirmationCode } from "./use-confirmation-code";

export default function ConfirmationCode() {
  const { onConfirmCode, control, error, isLoading, isButtonDisabled, user } =
    useConfirmationCode();

  return (
    <PageTemplate>
      <Heading size={700}>Confirmation code</Heading>
      <Text color="gray600" size={500}>
        Hey, {user?.name}. Please enter confirmation code sent to {user?.email}
      </Text>
      <Form onSubmit={onConfirmCode} gap={30} alignItems="center">
        <Controller
          name="code"
          control={control}
          render={({ field: { value, onChange } }) => (
            <CodeInput
              size={CODE_LENGTH}
              value={value}
              onChange={onChange}
              isError={!!error}
            />
          )}
        />
        {error && <ErrorText color="red500">{error}</ErrorText>}
        <ButtonContainer>
          <Button appearance="primary" disabled={isLoading || isButtonDisabled}>
            Continue
          </Button>
        </ButtonContainer>
      </Form>
    </PageTemplate>
  );
}
