import { useEffect, useMemo } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";

import { useAuth } from "../../auth";
import { MAIN_PATH, REGISTER_PATH } from "../../constants";
import { useConfirm, useDocumentTitle } from "../../hooks";
import { CODE_LENGTH } from "./constants";
import { ConfirmationCodeForm } from "./types";

export function useConfirmationCode() {
  useDocumentTitle("Confirmation code required");
  const navigate = useNavigate();
  const { isAuthenticated, user, setIsAuthenticated } = useAuth();
  const { error, resetError, checkCode, isPassed, isLoading } = useConfirm();
  const { control, handleSubmit, watch } = useForm<ConfirmationCodeForm>();

  const codeValue = watch("code", "");

  useEffect(() => {
    if (isPassed && setIsAuthenticated) {
      setIsAuthenticated(true);
    }
  }, [isPassed, setIsAuthenticated]);

  useEffect(() => {
    if (!user) {
      navigate(REGISTER_PATH);
    }
  }, [navigate, user]);

  useEffect(() => {
    if (user && isAuthenticated) {
      navigate(MAIN_PATH);
    }
  }, [isAuthenticated, navigate, user]);

  const handleConfirmCode = (data: ConfirmationCodeForm) => {
    resetError();
    checkCode(data.code).then();
  };

  const isButtonDisabled = useMemo(
    () => codeValue.replaceAll(" ", "").length !== CODE_LENGTH,
    [codeValue],
  );

  return {
    control,
    onConfirmCode: handleSubmit(handleConfirmCode),
    error,
    isLoading,
    isButtonDisabled,
    user,
  };
}
