import styled from "@emotion/styled";
import { Text } from "evergreen-ui";

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 300px;
  margin: 0 auto;
`;

export const ErrorText = styled(Text)`
  text-align: center;
`;
