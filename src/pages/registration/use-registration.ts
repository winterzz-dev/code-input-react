import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";

import { useAuth } from "../../auth";
import { CONFIRM_CODE_PATH, MAIN_PATH } from "../../constants";
import { useDocumentTitle } from "../../hooks";
import { RegisterForm } from "./types";
import { schema } from "./validation";

export function useRegistration() {
  useDocumentTitle("Registration required");
  const navigate = useNavigate();
  const { isAuthenticated, user, setUser } = useAuth();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisterForm>({
    resolver: yupResolver(schema),
  });

  if (isAuthenticated && user) {
    navigate(MAIN_PATH);
  }

  const handleRegister = (data: RegisterForm) => {
    if (setUser) {
      setUser(data);
      navigate(CONFIRM_CODE_PATH);
    }
  };

  return {
    register,
    onRegister: handleSubmit(handleRegister),
    errors,
  };
}
