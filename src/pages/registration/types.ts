export type RegisterForm = {
  name: string;
  email: string;
};
