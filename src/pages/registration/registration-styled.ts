import styled from "@emotion/styled";

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 300px;
  margin: 0 auto;
`;
