import * as yup from "yup";

export const schema = yup.object({
  name: yup.string().required().min(2).max(20),
  email: yup.string().required().email(),
});
