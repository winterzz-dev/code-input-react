import { Button, Heading, TextInputField } from "evergreen-ui";
import React from "react";

import { Form, PageTemplate } from "../../components";
import { ButtonContainer } from "./registration-styled";
import { useRegistration } from "./use-registration";

export default function Registration() {
  const { register, onRegister, errors } = useRegistration();

  return (
    <PageTemplate>
      <Heading size={700}>Registration</Heading>
      <Form onSubmit={onRegister}>
        <TextInputField
          label="Your name"
          placeholder="Type your name here"
          isInvalid={!!errors.name}
          validationMessage={errors.name?.message}
          {...register("name")}
        />
        <TextInputField
          label="Your email"
          placeholder="Type your email here"
          isInvalid={!!errors.email}
          validationMessage={errors.email?.message}
          {...register("email")}
        />
        <ButtonContainer>
          <Button appearance="primary">Continue</Button>
        </ButtonContainer>
      </Form>
    </PageTemplate>
  );
}
