import { Heading, Paragraph, Text } from "evergreen-ui";
import React from "react";

import { PageTemplate } from "../../components";
import { LinkToMain } from "./no-match-styled";
import { useNoMatch } from "./use-no-match";

function NoMatch() {
  const { redirectPath } = useNoMatch();

  return (
    <PageTemplate>
      <Heading size={900}>Page not found</Heading>
      <Paragraph size={400} color="gray600">
        <LinkToMain to={redirectPath}>
          <Text color="blue500" textDecoration="none" size={500}>
            Go to main page
          </Text>
        </LinkToMain>
      </Paragraph>
    </PageTemplate>
  );
}

export default NoMatch;
