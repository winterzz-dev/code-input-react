import { useAuth } from "../../auth";
import { MAIN_PATH, REGISTER_PATH } from "../../constants";
import { useDocumentTitle } from "../../hooks";

export function useNoMatch() {
  useDocumentTitle("Page not found");
  const { isAuthenticated } = useAuth();
  const redirectPath = isAuthenticated ? MAIN_PATH : REGISTER_PATH;

  return {
    redirectPath,
  };
}
