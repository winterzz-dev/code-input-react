import styled from "@emotion/styled";
import { Link } from "react-router-dom";

export const LinkToMain = styled(Link)`
  text-decoration: none;
`;
