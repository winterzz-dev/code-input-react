import { JSXElementConstructor } from "react";

export type AppRoute = {
  path: string;
  element: JSXElementConstructor<any> | string;
};
