export type ChildWithFocus = ChildNode & { focus: () => void };

export type ComponentProps = {
  size: number;
  value: string;
  onChange: (value: string) => void;
  isError?: boolean;
};
export type HookParams = Omit<ComponentProps, "isError">;
