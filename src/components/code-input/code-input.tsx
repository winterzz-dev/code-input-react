import React from "react";

import { Container, Input } from "./code-input-styled";
import { ComponentProps } from "./types";
import { useCodeInput } from "./use-code-input";

export function CodeInput({ size, value, onChange, isError }: ComponentProps) {
  const { containerRef, onInputChange, onKeyUp } = useCodeInput({
    value,
    onChange,
    size,
  });

  return (
    <Container ref={containerRef}>
      {new Array(size).fill(null).map((_value, index) => (
        <Input
          key={index}
          spellCheck={false}
          onChange={onInputChange}
          onKeyUp={onKeyUp}
          isInvalid={isError}
        />
      ))}
    </Container>
  );
}
