export const ON_PASTE_KEYBOARD_EVENT = "keydown";
export const V_KEY_CODE = "KeyV";
export const CLIPBOARD_ERROR_MESSAGE = "Access to clipboard denied";
