import { KeyboardEvent as ReactKeyboardEvent, RefObject } from "react";

import { ChildWithFocus } from "./types";

const isInputElement = (
  item: Element | HTMLInputElement,
): item is HTMLInputElement => {
  return !!(item as HTMLInputElement).value;
};

export const isFocusableChildNode = (
  node: ChildNode | null,
): node is ChildWithFocus => {
  return "focus" in (node || {});
};

export const getValue = (node: RefObject<HTMLDivElement>) => {
  const children = Array.from(node.current?.children || []);

  return children.reduce((acc: string, element: Element | HTMLInputElement) => {
    if (isInputElement(element)) return acc.concat(element.value);
    return acc.concat(" ");
  }, "");
};

export const setValues = (node: RefObject<HTMLDivElement>, value: string) => {
  const children = Array.from(node.current?.children || []);
  const symbols = value.split("");

  for (const letterIndex in symbols) {
    (children[letterIndex] as HTMLInputElement).value = symbols[letterIndex];
  }
};

export const backspaceHandler = (e: ReactKeyboardEvent<HTMLInputElement>) => {
  const target = e.currentTarget;
  const prevSibling = target.previousSibling;
  const isBackspace = e.code === "Backspace";
  const isEmpty = target.value === "";

  if (
    isBackspace &&
    isEmpty &&
    prevSibling &&
    isFocusableChildNode(prevSibling)
  ) {
    prevSibling.focus();
  }
};
