import { toaster } from "evergreen-ui";
import {
  ChangeEvent,
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
} from "react";

import {
  CLIPBOARD_ERROR_MESSAGE,
  ON_PASTE_KEYBOARD_EVENT,
  V_KEY_CODE,
} from "./contsants";
import { HookParams } from "./types";
import {
  backspaceHandler,
  getValue,
  isFocusableChildNode,
  setValues,
} from "./utils";

export function useCodeInput({ onChange, size, value = "" }: HookParams) {
  const containerRef = useRef<HTMLDivElement>(null);

  const changeHandler = (e: ChangeEvent<HTMLInputElement>) => {
    const currentValue = e.currentTarget.value || "";
    const { nextSibling, previousSibling } = e.currentTarget;

    if (currentValue.length === 0) {
      if (previousSibling && isFocusableChildNode(previousSibling)) {
        onChange(getValue(containerRef));
        return previousSibling.focus();
      }

      onChange(getValue(containerRef));
      return null;
    }

    if (currentValue.length > 1) {
      e.currentTarget.value = currentValue.slice(-1).toUpperCase();
    } else {
      e.currentTarget.value = currentValue.toUpperCase();
    }

    onChange(getValue(containerRef));

    if (nextSibling && isFocusableChildNode(nextSibling)) {
      return nextSibling.focus();
    }

    return null;
  };

  const pasteHandler = useCallback(
    async (ev: KeyboardEvent) => {
      try {
        const key = ev.code === V_KEY_CODE;
        const ctrl = ev.ctrlKey || ev.metaKey;

        if (key && ctrl) {
          const data = await navigator.clipboard.readText();

          const pasteText = data.length > size ? data.slice(0, size) : data;
          onChange(pasteText);
        }
      } catch (e) {
        toaster.danger(CLIPBOARD_ERROR_MESSAGE);
      }
    },
    [onChange, size],
  );

  useEffect(() => {
    const container = containerRef.current;

    if (container) {
      container.addEventListener(ON_PASTE_KEYBOARD_EVENT, pasteHandler);
    }

    return () => {
      if (container)
        container.removeEventListener(ON_PASTE_KEYBOARD_EVENT, pasteHandler);
    };
  });

  useEffect(() => {
    setValues(containerRef, value);
  }, [value]);

  useLayoutEffect(() => {
    if (containerRef.current) {
      const [firstLetterInput] = Array.from(containerRef.current.children);

      if (isFocusableChildNode(firstLetterInput)) {
        firstLetterInput.focus();
      }
    }
  }, [containerRef]);

  return {
    containerRef,
    onInputChange: changeHandler,
    onKeyUp: backspaceHandler,
  };
}
