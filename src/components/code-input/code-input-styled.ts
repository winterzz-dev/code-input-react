import styled from "@emotion/styled";
import { TextInput } from "evergreen-ui";

export const Container = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
`;

export const Input = styled(TextInput)`
  width: 60px;
  height: 60px;
  text-align: center;
  font-size: 22px;
  font-weight: 700;
`;
