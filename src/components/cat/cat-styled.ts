import styled from "@emotion/styled";

export const StyledImage = styled.img`
  max-width: 100%;
  height: auto;
`;
