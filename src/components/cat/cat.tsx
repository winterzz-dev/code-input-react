import React from "react";

import cat from "./cat-image.gif";
import { StyledImage } from "./cat-styled";

export function Cat() {
  return <StyledImage alt="cat" src={cat} />;
}
