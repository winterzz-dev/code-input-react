import { Heading, Paragraph, Spinner } from "evergreen-ui";
import React from "react";

import { PageTemplate } from "../page-template";

export function LoaderScreen() {
  return (
    <PageTemplate>
      <Heading size={900}>Loading...</Heading>
      <Spinner marginX="auto" />
      <Paragraph size={400} color="gray600">
        Please wait
      </Paragraph>
    </PageTemplate>
  );
}
