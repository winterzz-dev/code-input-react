import styled from "@emotion/styled";
import { motion } from "framer-motion";

export const LoaderWrapper = styled(motion.div)`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  gap: 50px;
`;
