import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { motion } from "framer-motion";

import { Breakpoint } from "../../constants";
import { forDevice } from "../../utils";

export const PageWrapper = styled(motion.div)`
  display: flex;
  flex-direction: column;
  gap: 50px;
  align-items: center;
  justify-content: center;
  width: 100%;
  max-width: 100vw;
  min-height: 100vh;

  padding: 50px 30px;

  ${forDevice(
    Breakpoint.tablet,
    css`
      padding: 100px 400px;
    `,
  )}
`;
