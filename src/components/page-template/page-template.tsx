import React, { PropsWithChildren } from "react";

import { DEFAULT_SCREEN_ANIMATION } from "../../constants";
import { Notification } from "./notification";
import { PageWrapper } from "./page-template-styled";

export function PageTemplate({ children }: PropsWithChildren<{}>) {
  return (
    <PageWrapper
      initial="from"
      animate="to"
      exit="out"
      variants={DEFAULT_SCREEN_ANIMATION}
    >
      <Notification />
      {children}
    </PageWrapper>
  );
}
