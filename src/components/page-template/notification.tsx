import { Alert, Link, Pane, Paragraph } from "evergreen-ui";
import React from "react";

export function Notification() {
  return (
    <Pane>
      <Alert
        intent="none"
        title="This app is demo for programming tutorial"
        marginBottom={32}
      >
        <Paragraph marginTop={10}>
          You can use any valid credentials and REACT confirmation code. See
          source code at:{" "}
          <Link
            target="_blank"
            href="https://gitlab.com/winterzz-dev/code-input-react"
          >
            Gitlab repository.
          </Link>
        </Paragraph>
        <Paragraph>
          This tutorial about how to create input-code component and integrate
          with react-hook-form
        </Paragraph>
      </Alert>
    </Pane>
  );
}
