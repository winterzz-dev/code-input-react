import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { CSSProperties } from "react";

export const Form = styled.form<{
  gap?: number;
  alignItems?: CSSProperties["alignItems"];
}>`
  display: flex;
  flex-direction: column;
  gap: ${({ gap = 10 }) => gap}px;
  width: 100%;
  ${({ alignItems }) =>
    alignItems &&
    css`
      align-items: ${alignItems};
    `}
`;
