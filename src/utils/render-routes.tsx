import React from "react";
import { Route } from "react-router-dom";

import { AppRoute } from "../types";

export const RenderRoutes = (routes: AppRoute[]): JSX.Element[] =>
  routes.map(({ path, element: Component }: AppRoute) => (
    <Route path={path} key={path} element={<Component />} />
  ));
