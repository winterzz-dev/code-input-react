import { css } from "@emotion/react";

import { Breakpoint, device } from "../constants";

export const forDevice = (
  breakpoint: Breakpoint,
  content: ReturnType<typeof css>,
): ReturnType<typeof css> => css`
  @media ${device[breakpoint]} {
    ${content}
  }
`;
