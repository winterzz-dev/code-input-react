export enum Breakpoint {
  mobileS = "320px",
  tablet = "768px",
}

export const device: Record<Breakpoint, string> = {
  [Breakpoint.mobileS]: `(min-width: ${Breakpoint.mobileS})`,
  [Breakpoint.tablet]: `(min-width: ${Breakpoint.tablet})`,
};
