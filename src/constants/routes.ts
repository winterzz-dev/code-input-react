import { lazy } from "react";

import RegistrationPage from "../pages/registration/registration";
import { AppRoute } from "../types";

const MainPage = lazy(() => import("../pages/main/main"));
const ConfirmationCodePage = lazy(
  () => import("../pages/confirmation-code/confirmation-code"),
);
const NoMatchPage = lazy(() => import("../pages/no-match/no-match"));

export const REGISTER_PATH = "/register";
export const MAIN_PATH = "/";
export const CONFIRM_CODE_PATH = "/confirm-code";
const FALLBACK_PATH = "/*";

export const ROUTES: AppRoute[] = [
  {
    path: MAIN_PATH,
    element: MainPage,
  },
  {
    path: REGISTER_PATH,
    element: RegistrationPage,
  },
  {
    path: CONFIRM_CODE_PATH,
    element: ConfirmationCodePage,
  },
  {
    path: FALLBACK_PATH,
    element: NoMatchPage,
  },
];
