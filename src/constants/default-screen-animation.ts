export const DEFAULT_SCREEN_ANIMATION = {
  from: {
    opacity: 0.7,
    scale: 0.8,
  },
  to: {
    opacity: 1,
    scale: 1,
    transition: {
      ease: "easeOut",
    },
  },
  out: {
    opacity: 0.7,
    scale: 0.8,
    transition: {
      ease: "easeIn",
    },
  },
};
