import { AnimatePresence } from "framer-motion";
import React, { Suspense } from "react";
import { BrowserRouter, Routes } from "react-router-dom";

import { LoaderScreen } from "../components";
import { ROUTES } from "../constants";
import { RenderRoutes } from "../utils";

export function Frame() {
  return (
    <AnimatePresence>
      <Suspense fallback={<LoaderScreen />}>
        <BrowserRouter>
          <Routes>{RenderRoutes(ROUTES)}</Routes>
        </BrowserRouter>
      </Suspense>
    </AnimatePresence>
  );
}
