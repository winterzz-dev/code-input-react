import "./reset.css";

import React from "react";
import ReactDOM from "react-dom/client";

import { AuthProvider } from "./auth";
import { Frame } from "./frame";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <AuthProvider>
      <Frame />
    </AuthProvider>
  </React.StrictMode>,
);
