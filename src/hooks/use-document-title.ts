import { useEffect, useState } from "react";

export function useDocumentTitle(value: string) {
  const [title, setTitle] = useState<string>(value);

  useEffect(() => {
    document.title = title;
  }, [title]);

  return {
    title,
    setTitle,
  };
}
