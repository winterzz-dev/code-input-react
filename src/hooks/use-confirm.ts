import { useState } from "react";

const ALLOWED_CODES = ["REACT"];

export function useConfirm() {
  const [isPassed, setIsPassed] = useState<boolean>(false);
  const [isLoading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  const resetError = () => {
    setError(null);
  };

  const checkCode = async (code: string) => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      if (ALLOWED_CODES.includes(code)) {
        return setIsPassed(true);
      }
      return setError("Wrong confirmation code");
    }, 2000);
  };

  return {
    isLoading,
    isPassed,
    error,
    resetError,
    checkCode,
  };
}
