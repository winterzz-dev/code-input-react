### Code Input React [![Netlify Status](https://api.netlify.com/api/v1/badges/2bd20922-992d-4f35-8a8b-2527cad841e0/deploy-status)](https://app.netlify.com/sites/winterzz-dev-code-input/deploys)

---

*Руководство на других языках: [English](README.md)*

----

[Demo](https://winterzz-dev-code-input.netlify.app)

---

Это репозиторий с исходным кодом для урока по программированию на `React`.

- Мы создадим компонент для ввода кода подтверждение
- Интегрируем его с библиотекой `react-hook-form`

---

![Image screen](https://i.postimg.cc/dVYWhX2V/SCR-20220723-nbe.png)

---

Для авторизации можно использовать любые имя, адрес электронной почты и код *REACT* (приложение
примет только этот код, поскольку оно полностью изолировано и данные никуда не отправляются,
проверка кода эмулируется)
